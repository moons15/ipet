from django.contrib.auth.models import User
from django.db import models
from django.db.models import *
from apps.pets.models import *
from apps.accounts import *

class MatchPet(models.Model):
    pet = models.ForeignKey(Pet,max_length=100, blank=True, null=True, verbose_name='Nombre de la mascota')
    is_like = models.BooleanField(default=False, verbose_name='¿Le gusta?')


    class Meta:

        verbose_name = "Match Pet"

    def __str__(self):
        return self.pet