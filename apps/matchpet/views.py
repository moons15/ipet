from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import generics,viewsets
from .serializers import *
from .models import *
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView

class RetrieveMatchPetAPIView(generics.RetrieveAPIView):

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = MatchPet.objects.all()
    serializers_class = PetMatchSerializer

