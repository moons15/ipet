from django.apps import AppConfig


class MatchPetConfig(AppConfig):
    name = 'apps.matchpet'
