from rest_framework import serializers
from .models import *

class PetMatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pet
        fields = ['pet','match']
