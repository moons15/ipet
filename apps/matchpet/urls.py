from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from apps.matchpet.views import *
from django.contrib.auth import views
from rest_framework import generics

urlpatterns = [
    url(r'^matchpet/(?P<pk>\d+)/$', RetrieveMatchPetAPIView.as_view()),
]