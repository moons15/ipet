from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from .views import *
from django.contrib.auth import views

urlpatterns = [
    url(r'^register/$', CreateUserAPIView.as_view()),
    url(r'^user/retrieve/$', RetrieveUserAPIView.as_view()),
    url(r'^user/change-password/$', ChangePasswordAPIView.as_view()),
    url(r'^login/$', LoginAPIView.as_view()),
    url(r'^login/mobile/(?P<backend>[^/]+)/$', FacebookMobileLoginAPI.as_view(), name="facebook-mobile-login"),
    url(r'^user/update/$', UpdateUserAPIView.as_view()),
    url(r'^user/(?P<pk>\d+)/photo/$', UpdateUserPhotoAPIView.as_view()),
    url(
        r'^user/recovery/(?P<id>\d+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        {"post_reset_redirect": reverse_lazy(
            'account:password_reset_complete')},
        name='password_reset_confirm'),
    url(r'^reset/done/$', password_reset_complete,
        name='password_reset_complete'),
    url(r'^user/recovery/$', RecoveryPasswordAPI.as_view()),

]
