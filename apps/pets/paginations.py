from rest_framework.pagination import PageNumberPagination

__author__ = 'richard'


class TwelvePetsPagination(PageNumberPagination):
    page_size = 12

