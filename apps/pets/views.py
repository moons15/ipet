from rest_framework import generics, status, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from django.contrib import messages
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from apps.accounts.models import User
from apps.pets.paginations import TwelvePetsPagination
from apps.pets.serializers import *

class PetRegisterAPIView(generics.ListAPIView):

    pagination_class = PageNumberPagination
    serializer_class = PetSerializer
    queryset = Pet.objects.all()


class PetDeleteAPIView(generics.DestroyAPIView):

    pagination_class = PageNumberPagination
    serializer_class = PetSerializer
    queryset = Pet.objects.all()

    def delete(self, request, *args, **kwargs):
        key=self.kwargs('pk')
        objeto = Pet.objects.get(pk=key)
        objeto.delete()

class PetLostAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated)

    def post(self, request, *args, **kwargs):
        pet = get_object_or_404(self.request.user.pets, id=self.kwargs.get('pk'))
        pet.message_lost = self.request.data.get('message_lost', 'No hay información extra')
        pet.is_lost = True
        pet.save()
        return Response({'detail': 'Mascota perdida'}, status=status.HTTP_200_OK)

class PetFoundAPI(generics.ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        pet = get_object_or_404(self.request.user.pets, id=self.kwargs.get('pk'))
        pet.is_lost = False
        pet.save()
        return Response({'detail': 'Mascota encontrada'}, status=status.HTTP_200_OK)

class ListPetAdoptionAPI(generics.ListAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    queryset = Pet.objects.all()

class PetUpdateAPIView(generics.RetrieveUpdateAPIView):

    queryset = Pet.objects.all()
    serializer_class = PetSerializer

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class ListPetLostView(generics.ListAPIView):
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = ListPetLostSerializer
    pagination_class = TwelvePetsPagination

    def get_queryset(self):
        return Pet.objects.filter(is_lost=True).select_related('user').order_by('?')
