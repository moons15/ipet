
from rest_framework import serializers
from apps.accounts.serializers import UserInPetSerializer
from apps.pets.models import *


class PetSerializer(serializers.ModelSerializer):
    user = UserInPetSerializer(read_only=True)

    class Meta:
        model = Pet
        fields = ['id', 'name', 'photo',  'gender',
                  'breed', 'age','description', 'user', 'qrcode']

class ListPetLostSerializer(serializers.ModelSerializer):
    user = UserInPetSerializer(read_only=True)

    class Meta:
        model = Pet
        fields = ['id', 'name', 'photo', 'address',
                  'description', 'user', 'qrcode']

class ListPetAdoptionSerializer(serializers.ModelSerializer):
    user = UserInPetSerializer(read_only=True)

    class Meta:
        model = Pet
        fields = ['id', 'name', 'photo', 'description', ]