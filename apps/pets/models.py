from ..accounts.models import User
from django.db import models
from enum import Enum
from django.db.models import *

class Kind_animal(models.Model):

    name = models.CharField(max_length=100, verbose_name='Nombre')
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_enable = models.BooleanField(default=True)


    class Meta:
        verbose_name_plural = "Tipos de animales"
        verbose_name = "Tipo de animal"
        ordering = ['name', ]

    def __str__(self):
        return self.name

class Breed(models.Model):

    kind_animal = models.ForeignKey(Kind_animal, related_name='pets', verbose_name='Tipo')
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100, verbose_name='Nombre')


    class Meta:
        verbose_name_plural = "Razas"
        verbose_name = "Raza"
        ordering = ['name', ]

    def __str__(self):
        return self.name


class Pet(models.Model):

    class Gender(Enum):
        m = "Masculino"
        f = "Femenino"

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='pets', verbose_name='Dueño')
    photo = models.ImageField(upload_to='pets/image', blank=True, null=True, verbose_name="Imagen")
    name = models.CharField(max_length=100, verbose_name='Nombre')
    gender = models.CharField(max_length=1, blank=True, choices=[(item.name, item.value) for item in Gender])
    breed = models.ForeignKey(Breed, related_name='pets', verbose_name='Raza')
    age = models.DateField(blank=True, null=True, verbose_name='Fecha de nacimiento')
    description = models.TextField(blank=True, null=True, verbose_name='Descripción')
    is_lost = models.BooleanField(default=False, verbose_name='Está perdido?')
    qrcode = models.ImageField(upload_to='qrcode', blank=True, null=True)

    class Meta:
        verbose_name_plural = "Mascotas"
        verbose_name = "Mascota"
        ordering = ['-created_at', ]

    def __str__(self):
        return self.name