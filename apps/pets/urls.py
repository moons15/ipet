
from django.conf.urls import url
from apps.pets.views import *
from apps.pets.serializers import *
from rest_framework import generics
from django.contrib.auth import views

urlpatterns = [
      url(r'^pet/register/$', PetRegisterAPIView.as_view()),
      url(r'^pet/delete/(?P<pk>\d+)/$', PetDeleteAPIView.as_view()),
      url(r'^pet/lost/(?P<pk>\d+)/$', PetLostAPI.as_view()),
      url(r'^pet/found/(?P<pk>\d+)/$', PetFoundAPI.as_view()),
      url(r'^pet/adoption/$', ListPetAdoptionAPI.as_view()),
      url(r'^pet/update/(?P<pk>\d+)/$', PetUpdateAPIView.as_view()),
     # url(r'^pet/listfound/$', ListPetLostSerializer.as_view()),
]