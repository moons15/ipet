from django.conf.urls import url
from .views import *
from django.contrib.auth import views

urlpatterns = [
    url(r'^vets/$', ListVetAPIView.as_view())
]
