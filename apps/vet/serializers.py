from .models import *
from rest_framework import serializers


class VetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vet
        fields = ('id', 'name', 'address','mobile','photo')

