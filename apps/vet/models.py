from django.contrib.auth.models import User
from django.db import models
from django.db.models import *

class Vet(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_enable = models.BooleanField(default=True)
    name = models.CharField(max_length=100, verbose_name='Nombre')
    address = models.CharField(max_length=100, verbose_name='Direccion')
    mobile = models.CharField(max_length=100, verbose_name='Celular')
    photo = models.ImageField(max_length=100, verbose_name='Imagen')

    class Meta:
        verbose_name_plural = "Veterinarias"
        verbose_name = "Veterianaria"
        ordering = ['name', ]

    def __str__(self):
        return self.name