from rest_framework import generics,viewsets
from .serializers import *
from .models import *

class ListVetAPIView(generics.RetrieveAPIView):

    queryset = Vet.objects.all()
    serializers_class = VetSerializer